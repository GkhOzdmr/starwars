package trivago.starwars.android.features.character.domain.usecase

import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import trivago.starwars.android.core.interactor.UseCase
import trivago.starwars.android.features.character.data.entity.CharDetailsEntity
import trivago.starwars.android.features.character.data.entity.FilmDetailsEntity
import trivago.starwars.android.features.character.data.entity.PlanetDetailsEntity
import trivago.starwars.android.features.character.data.entity.SpecieDetailsEntity
import trivago.starwars.android.features.character.data.repository.CharSearchRepository
import trivago.starwars.android.features.character.domain.model.CharSearchFinalModel
import javax.inject.Inject

class GetPerson @Inject constructor(
    private val charSearchRepository: CharSearchRepository
) : UseCase<CharSearchFinalModel, GetPerson.Params>() {

    override fun buildUseCaseObservable(params: Params): Observable<CharSearchFinalModel> {
        return charSearchRepository.getPerson(params.personNo)
            .subscribeOn(Schedulers.io())
            .flatMap { charDetailsEntity: CharDetailsEntity ->
                Observable.zip(
                    charSearchRepository.getFilm("2").doOnTerminate{ Timber.d("getFilm doOnTerminate")},
                    charSearchRepository.getSpecie(charDetailsEntity.species[0].toCharArray().lastIndex.toString()).doOnTerminate{ Timber.d("getSpecie doOnTerminate")},
                    charSearchRepository.getPlanet(charDetailsEntity.homeworld.toCharArray().lastIndex.toString()).doOnTerminate{ Timber.d("getPlanet doOnTerminate")},
                    Function3<FilmDetailsEntity, SpecieDetailsEntity, PlanetDetailsEntity, CharSearchFinalModel> { t1, t2, t3 ->
                        CharSearchFinalModel(
                            charDetailsEntity.name,
                            charDetailsEntity.birth_year,
                            charDetailsEntity.height,
                            t3.mapToModel(),
                            t2.mapToModel(),
                            listOf(t1.mapToModel())
                        )
                    })
            }
    }

    data class Params(val personNo: String)

}
