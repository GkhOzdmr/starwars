package trivago.starwars.android.features.character.data.entity

import com.google.gson.annotations.SerializedName

data class CharSearchResultEntity(
    @SerializedName("count") val count : Int,
    @SerializedName("next") val next : String,
    @SerializedName("previous") val previous : String,
    @SerializedName("results") val results : List<CharDetailsEntity>
)