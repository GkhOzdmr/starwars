package trivago.starwars.android.features.character.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import trivago.starwars.android.features.character.data.entity.PlanetDetailsEntity

data class PlanetDetailsModel(
    val name: String,
    val population: Long
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readLong()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeLong(population)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlanetDetailsModel> {
        override fun createFromParcel(parcel: Parcel): PlanetDetailsModel {
            return PlanetDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<PlanetDetailsModel?> {
            return arrayOfNulls(size)
        }
    }
}