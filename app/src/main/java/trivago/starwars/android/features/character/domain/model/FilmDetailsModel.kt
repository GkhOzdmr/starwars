package trivago.starwars.android.features.character.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import trivago.starwars.android.features.character.data.entity.FilmDetailsEntity

data class FilmDetailsModel(
    val title: String,
    val release_date: String,
    val opening_crawl: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(release_date)
        parcel.writeString(opening_crawl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilmDetailsModel> {
        override fun createFromParcel(parcel: Parcel): FilmDetailsModel {
            return FilmDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<FilmDetailsModel?> {
            return arrayOfNulls(size)
        }
    }
}