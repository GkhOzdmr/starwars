package trivago.starwars.android.features.character.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import trivago.starwars.android.features.character.data.entity.SpecieDetailsEntity

data class SpecieDetailsModel(
    val name: String,
    val language: String,
    val homeworld: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(language)
        parcel.writeString(homeworld)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SpecieDetailsModel> {
        override fun createFromParcel(parcel: Parcel): SpecieDetailsModel {
            return SpecieDetailsModel(parcel)
        }

        override fun newArray(size: Int): Array<SpecieDetailsModel?> {
            return arrayOfNulls(size)
        }
    }
}