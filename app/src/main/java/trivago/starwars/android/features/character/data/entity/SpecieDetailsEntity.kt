package trivago.starwars.android.features.character.data.entity

import com.google.gson.annotations.SerializedName
import trivago.starwars.android.features.character.domain.model.SpecieDetailsModel

data class SpecieDetailsEntity(
    @SerializedName("name") val name: String,
    @SerializedName("language") val language: String,
    @SerializedName("homeworld") val homeworld: String
){
    fun mapToModel(): SpecieDetailsModel {
        return SpecieDetailsModel(
            name,
            language,
            homeworld
        )
    }
}