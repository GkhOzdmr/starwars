package trivago.starwars.android.features.character.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_characters.view.*
import trivago.starwars.android.R
import trivago.starwars.android.features.character.data.entity.CharSearchResultEntity
import trivago.starwars.android.features.character.domain.model.CharDetailsModel

class CharactersAdapter constructor(
    val context: Context,
    val items: List<CharDetailsModel>,
    val notifier: Notifier
) : RecyclerView.Adapter<CharactersAdapter.ViewHolder>() {

    interface Notifier {
        fun onCharacterSelected(characterDetailsUrl: String)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = items[position]
        holder.tv_char_name.text = currentItem.name
        holder.tv_char_birth.text = currentItem.birth_year
    }

    class ViewHolder(view: View, notifier: Notifier, items: List<CharDetailsModel>) : RecyclerView.ViewHolder(view) {
        val cl_item_characters = view.cl_item_characters
        val tv_char_name = view.tv_char_name
        val tv_char_birth = view.tv_char_birth
        val btn_go_to_char = view.btn_go_to_char

        init {
            btn_go_to_char.setOnClickListener {
                notifier.onCharacterSelected(items[adapterPosition].url)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_characters, parent, false),
            notifier,
            items
        )
    }

    override fun getItemCount(): Int = items.size
}