package trivago.starwars.android.features.character.data.repository

import android.annotation.SuppressLint
import io.reactivex.Observable
import io.reactivex.Single
import trivago.starwars.android.core.di.SERVICE_SWAPI
import trivago.starwars.android.core.service.SwapiApi
import trivago.starwars.android.features.character.data.entity.*
import trivago.starwars.android.features.character.domain.model.CharDetailsModel
import javax.inject.Inject
import javax.inject.Named

class CharSearchRepositoryImpl @Inject constructor(
    @Named(SERVICE_SWAPI) val swapiApi: SwapiApi
) : CharSearchRepository {

    override fun searchCharacter(name: String): Observable<CharSearchResultEntity> {
        return swapiApi.searchCharacter(name)
    }

    @SuppressLint("CheckResult")
    override fun getPerson(personNo: String): Observable<CharDetailsEntity> {
        return swapiApi.getPerson(personNo)

//            .flatMap {
//                val listOfFilms = mutableListOf<FilmDetailsModel>()
//                swapiApi.getFilm(it.films[0].toCharArray().lastIndex.toString()).subscribeOn(Schedulers.io())
//                    .subscribe { t: FilmDetailsEntity? ->
//                        listOfFilms.add(t!!.mapToModel())
//                    }
//                val films = Observable.just(listOfFilms.toList()).subscribeOn(Schedulers.io())
//                val specie = swapiApi.getSpecie(it.species[0].toCharArray().lastIndex.toString()).subscribeOn(Schedulers.io())
//                val planet = swapiApi.getPlanet(it.homeworld.toCharArray().lastIndex.toString()).subscribeOn(Schedulers.io())
//                Observable.zip(specie, planet, films,
//                    Function3<SpecieDetailsEntity, PlanetDetailsEntity, List<FilmDetailsModel>, CharSearchFinalModel> { t1, t2, t3 ->
//                        CharSearchFinalModel(it.name, it.birth_year, it.height, t2.mapToModel(), t1.mapToModel(), t3)
//                    })
//            }.subscribeOn(Schedulers.io())
    }

    override fun getFilm(filmNo: String): Observable<FilmDetailsEntity> {
        return swapiApi.getFilm(filmNo)
    }

    override fun getPlanet(planetNo: String): Observable<PlanetDetailsEntity> {
        return swapiApi.getPlanet(planetNo)
    }

    override fun getSpecie(specieNo: String): Observable<SpecieDetailsEntity> {
        return swapiApi.getSpecie(specieNo)
    }
}