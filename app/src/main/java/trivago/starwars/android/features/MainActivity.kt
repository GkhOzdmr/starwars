package trivago.starwars.android.features

import trivago.starwars.android.R
import trivago.starwars.android.core.platform.BaseActivity

/**
 * Base activity for SingleActivity app. Navigation controlled through Android JetPack Navigation tool.
 *
 * @see [R.navigation.nav_graph] for navigation graph.
 */
class MainActivity : BaseActivity() {

    override fun layoutId() = R.layout.activity_main

}
