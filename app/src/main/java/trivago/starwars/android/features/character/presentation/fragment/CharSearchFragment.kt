package trivago.starwars.android.features.character.presentation.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_search.*
import timber.log.Timber

import trivago.starwars.android.R
import trivago.starwars.android.core.exception.Failure
import trivago.starwars.android.core.extension.viewModel
import trivago.starwars.android.core.functional.Either
import trivago.starwars.android.core.functional.ViewState
import trivago.starwars.android.core.platform.BaseFragment
import trivago.starwars.android.features.character.data.repository.CharSearchRepository
import trivago.starwars.android.features.character.domain.model.CharDetailsModel
import trivago.starwars.android.features.character.presentation.adapter.CharactersAdapter
import trivago.starwars.android.features.character.presentation.viewmodel.CharacterViewModel
import javax.inject.Inject

class CharSearchFragment :
    BaseFragment(),
    CharactersAdapter.Notifier
{


    override fun layoutId(): Int = R.layout.fragment_search

    override fun onBackPressed(): Boolean = true

    lateinit var characterViewModel: CharacterViewModel
    lateinit var charDetailsList: List<CharDetailsModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun observeViewModel() {
        characterViewModel = viewModel(viewModelFactory) {
            viewState.observe(this@CharSearchFragment, Observer {
                onViewStateChange(it)
            })
            taskObserver.observe(this@CharSearchFragment, Observer {
                onTaskResult(it)
            })
        }
    }

    private fun initViews() {
        setListeners()
    }

    private fun setListeners() {
        edt_char_search.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(s.toString().isNotEmpty())
                    characterViewModel.searchCharacter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun onTaskResult(either: Either<Failure, CharacterViewModel.State>) {
        either.either({ failure ->
            Timber.d("failure:${failure.toString()}")
        }, { state: CharacterViewModel.State ->
            when (state) {
                is CharacterViewModel.State.LoadCharacters -> {
                    charDetailsList = (state as CharacterViewModel.State.LoadCharacters).list
                    initRecyclerView()
                }

                is CharacterViewModel.State.LoadCharacterDetails -> {
                    val model = (state as CharacterViewModel.State.LoadCharacterDetails).charSearchFinalModel
                    navigateTo(
                        R.id.action_toCharDetailsFragment,
                        Bundle().apply {
                            putParcelable(ARG_LIST_CHAR_FINAL_MODEL, model)
                        })
                }
            }
        })
    }

    private fun onViewStateChange(viewState: ViewState) {
        when (viewState) {
            is ViewState.ShowProgress -> {
                cl_progress.visibility = View.VISIBLE
            }
            is ViewState.HideProgress -> {
                cl_progress.visibility = View.GONE
            }
        }
    }

    private fun initRecyclerView() {
        rc_characters.apply {
            adapter = CharactersAdapter(context, charDetailsList, this@CharSearchFragment)
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onCharacterSelected(characterDetailsUrl: String) {
        characterViewModel.getPerson(characterDetailsUrl)
    }
}
