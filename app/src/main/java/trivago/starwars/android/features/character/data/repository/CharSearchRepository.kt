package trivago.starwars.android.features.character.data.repository

import io.reactivex.Observable
import trivago.starwars.android.features.character.data.entity.*

interface CharSearchRepository {

    fun searchCharacter(name: String): Observable<CharSearchResultEntity>

    fun getPerson(personNo: String): Observable<CharDetailsEntity>

    fun getFilm(filmNo: String): Observable<FilmDetailsEntity>

    fun getPlanet(planetNo: String): Observable<PlanetDetailsEntity>

    fun getSpecie(specieNo: String): Observable<SpecieDetailsEntity>
}