package trivago.starwars.android.features.character.domain.model

import trivago.starwars.android.features.character.data.entity.CharDetailsEntity

data class CharDetailsModel(
    val name: String,
    val height: Int,
    val mass: Int,
    val hair_color: String,
    val skin_color: String,
    val eye_color: String,
    val birth_year: String,
    val gender: String,
    val homeworld: String,
    val films: List<String>,
    val species: List<String>,
    val vehicles: List<String>,
    val starships: List<String>,
    val created: String,
    val edited: String,
    val url: String
)