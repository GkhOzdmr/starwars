package trivago.starwars.android.features.character.presentation.fragment

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_char_details.*
import timber.log.Timber
import trivago.starwars.android.R
import trivago.starwars.android.core.platform.BaseFragment
import trivago.starwars.android.features.character.domain.model.CharSearchFinalModel

const val ARG_LIST_CHAR_FINAL_MODEL = "ARG_LIST_CHAR_FINAL_MODEL"
class CharDetailsFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_char_details

    override fun onBackPressed(): Boolean = true

    private var charFinalModel: CharSearchFinalModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (arguments!!.getParcelable<CharSearchFinalModel>(ARG_LIST_CHAR_FINAL_MODEL) != null) {
                charFinalModel = arguments!!.getParcelable<CharSearchFinalModel>(ARG_LIST_CHAR_FINAL_MODEL)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        initLayout()
    }

    private fun initLayout() {
        if (charFinalModel != null) {
            tv_char_name.text = resources.getString(R.string.character_details_name, charFinalModel!!.charName)
            tv_char_birth_year.text = resources.getString(R.string.character_details_birth_year, charFinalModel!!.charBirthYear)
            tv_char_height.text = resources.getString(R.string.character_details_height, charFinalModel!!.charHeight)
            tv_char_language.text = resources.getString(R.string.character_details_language, charFinalModel!!.species?.language)
            tv_char_homeworld_name.text = resources.getString(R.string.character_details_homeworld_name, charFinalModel!!.planet?.name)
            tv_char_homeworld_population.text = resources.getString(R.string.character_details_homeworl_population,charFinalModel!!.planet?.population.toString())

            val films = if(charFinalModel!!.films != null){
                var string = ""

                for(film in charFinalModel!!.films!!) {
                    string += "\n" + resources.getString(R.string.character_details_film_details, film.title, film.release_date, film.opening_crawl)
                }

                string
            } else  ""

            tv_char_films.text = resources.getString(R.string.character_details_films, films)
        }
    }

}
