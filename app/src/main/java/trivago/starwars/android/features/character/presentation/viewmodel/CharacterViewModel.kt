package trivago.starwars.android.features.character.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import trivago.starwars.android.core.exception.Failure
import trivago.starwars.android.core.functional.Either
import trivago.starwars.android.core.functional.ViewState
import trivago.starwars.android.core.platform.BaseViewModel
import trivago.starwars.android.features.character.domain.model.CharDetailsModel
import trivago.starwars.android.features.character.domain.model.CharSearchFinalModel
import trivago.starwars.android.features.character.domain.usecase.GetPerson
import trivago.starwars.android.features.character.domain.usecase.SearchCharacters
import javax.inject.Inject

class CharacterViewModel @Inject constructor(
    private val searchCharacters: SearchCharacters,
    private val getPerson: GetPerson
) : BaseViewModel() {

    var viewState: MutableLiveData<ViewState> = MutableLiveData()
    var taskObserver: MutableLiveData<Either<Failure, State>> = MutableLiveData()

    fun searchCharacter(characterName: String) {
        viewState.value = ViewState.ShowProgress()
        disposable.add(
            searchCharacters.execute(SearchCharacters.Params(characterName))!!
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list ->
                    taskObserver.value = Either.Right<State>(State.LoadCharacters(list))
                    viewState.value = ViewState.HideProgress
                }, {t ->
                    viewState.value = ViewState.HideProgress
                    Timber.d("error: ${t?.localizedMessage}")
                })
        )
    }

    fun getPerson(characterDetailsUrl: String) {
        viewState.value = ViewState.ShowProgress()
        val personNo = characterDetailsUrl.lastIndex
        disposable.add(
            getPerson.execute(GetPerson.Params(personNo.toString()))!!
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ model ->
                    Timber.d("model.charName ${model.charName}")
                    taskObserver.value = Either.Right<State>(State.LoadCharacterDetails(model))
                    viewState.value = ViewState.HideProgress
                }, { t ->
                    Timber.d("error: ${t.localizedMessage}")
                    viewState.value = ViewState.HideProgress
                })
        )
    }

    sealed class State{
        class LoadCharacters(val list: List<CharDetailsModel>): State()
        class LoadCharacterDetails(val charSearchFinalModel: CharSearchFinalModel): State()
    }
}