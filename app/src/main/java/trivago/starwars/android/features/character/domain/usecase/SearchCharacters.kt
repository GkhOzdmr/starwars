package trivago.starwars.android.features.character.domain.usecase

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import trivago.starwars.android.core.interactor.UseCase
import trivago.starwars.android.features.character.data.entity.CharDetailsEntity
import trivago.starwars.android.features.character.data.repository.CharSearchRepository
import trivago.starwars.android.features.character.domain.model.CharDetailsModel
import javax.inject.Inject

class SearchCharacters @Inject constructor(
    private val charSearchRepository: CharSearchRepository
) : UseCase<List<CharDetailsModel>, SearchCharacters.Params>() {

    override fun buildUseCaseObservable(params: Params): Observable<List<CharDetailsModel>> {
        return charSearchRepository.searchCharacter(params.characterName)
            .subscribeOn(Schedulers.io())
            .map {
                Timber.d("original list size ${it.results.size}")
                val modelList = mutableListOf<CharDetailsModel>()
                it.results.forEach { charDetailsEntity: CharDetailsEntity ->
                    modelList.add(charDetailsEntity.mapToModel())
                }
                modelList.toList()
            }
    }

    data class Params(val characterName: String)
}
