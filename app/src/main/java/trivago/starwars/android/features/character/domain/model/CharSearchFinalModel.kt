package trivago.starwars.android.features.character.domain.model

import android.os.Parcel
import android.os.Parcelable

data class CharSearchFinalModel(
    val charName: String?,
    val charBirthYear: String?,
    val charHeight: Int,
    val planet: PlanetDetailsModel?,
    val species: SpecieDetailsModel?,
    val films: List<FilmDetailsModel>?
) : Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readParcelable(PlanetDetailsModel::class.java.classLoader),
        parcel.readParcelable(SpecieDetailsModel::class.java.classLoader),
        parcel.createTypedArrayList(FilmDetailsModel)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(charName)
        parcel.writeString(charBirthYear)
        parcel.writeInt(charHeight)
        parcel.writeParcelable(planet, flags)
        parcel.writeParcelable(species, flags)
        parcel.writeTypedList(films)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CharSearchFinalModel> {
        override fun createFromParcel(parcel: Parcel): CharSearchFinalModel {
            return CharSearchFinalModel(parcel)
        }

        override fun newArray(size: Int): Array<CharSearchFinalModel?> {
            return arrayOfNulls(size)
        }
    }

}