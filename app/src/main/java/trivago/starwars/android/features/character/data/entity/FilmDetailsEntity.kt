package trivago.starwars.android.features.character.data.entity

import com.google.gson.annotations.SerializedName
import trivago.starwars.android.features.character.domain.model.FilmDetailsModel

data class FilmDetailsEntity(
    @SerializedName("title") val title: String,
    @SerializedName("release_date") val release_date: String,
    @SerializedName("opening_crawl") val opening_crawl: String
){
    fun mapToModel(): FilmDetailsModel {
        return FilmDetailsModel(
            title,
            release_date,
            opening_crawl
        )
    }
}