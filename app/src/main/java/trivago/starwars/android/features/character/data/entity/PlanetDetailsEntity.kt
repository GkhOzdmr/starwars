package trivago.starwars.android.features.character.data.entity

import com.google.gson.annotations.SerializedName
import trivago.starwars.android.features.character.domain.model.PlanetDetailsModel

data class PlanetDetailsEntity(
    @SerializedName("name") val name: String,
    @SerializedName("population") val population: Long
){
    fun mapToModel(): PlanetDetailsModel {
        return PlanetDetailsModel(
            name,
            population
        )
    }
}