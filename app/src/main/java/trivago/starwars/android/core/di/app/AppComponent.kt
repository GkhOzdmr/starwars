package trivago.starwars.android.core.di.app

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import trivago.starwars.android.core.platform.StarWarsApp

/**
 * Dagger2 Component for injecting modules to application
 */
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuilder::class,
    FragmentBuilder::class,
    AppModule::class
])
interface AppComponent : AndroidInjector<StarWarsApp> {

    /**
     * Injects dependencies with declared modules
     */
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: StarWarsApp) : Builder
        fun build() : AppComponent
    }

}