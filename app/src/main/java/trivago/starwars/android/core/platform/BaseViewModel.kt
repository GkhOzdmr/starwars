package trivago.starwars.android.core.platform

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import trivago.starwars.android.core.exception.Failure

/**
 * Base ViewModel class with default Failure handling.
 * @see ViewModel
 * @see Failure
 */
abstract class BaseViewModel : ViewModel() {

    internal var disposable = CompositeDisposable()

    override fun onCleared() {
        disposeSubscriptions()
        super.onCleared()
    }

    /**
     * Disposes un-disposed subscriptions, should be called at onStop/onDestroy lifecycle state
     */
    private fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.dispose()
    }

    var failure: MutableLiveData<Failure> = MutableLiveData()

    protected fun handleFailure(failure: Failure) {
        this.failure.value = failure
    }

}