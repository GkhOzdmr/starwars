package trivago.starwars.android.core.di

/**
 * Constant values to be used in Dagger2 @Named annotations
 */

const val SERVICE_SWAPI = "SERVICE_SWAPI"
const val API_SWAPI = "API_SWAPI"