package trivago.starwars.android.core.di.data

import dagger.Module
import dagger.Provides
import trivago.starwars.android.core.di.SERVICE_SWAPI
import trivago.starwars.android.core.di.annotations.ActivityScope
import trivago.starwars.android.core.service.SwapiApi
import trivago.starwars.android.features.character.data.repository.CharSearchRepository
import trivago.starwars.android.features.character.data.repository.CharSearchRepositoryImpl
import javax.inject.Named

/**
 * Dagger2 Module for providing repository interface implementor instances
 */
@Module
class RepositoryModule {

    @ActivityScope
    @Provides
    fun provideProdLinksRepository(@Named(SERVICE_SWAPI) swapiApi: SwapiApi):
            CharSearchRepository = CharSearchRepositoryImpl(swapiApi)

}

