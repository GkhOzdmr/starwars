package trivago.starwars.android.core.di.data

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.create
import trivago.starwars.android.core.di.API_SWAPI
import trivago.starwars.android.core.di.SERVICE_SWAPI
import trivago.starwars.android.core.di.annotations.ActivityScope
import trivago.starwars.android.core.service.SwapiApi
import javax.inject.Named

/**
 * Dagger 2 Module for providing endpoint Services
 */
@Module
class ServiceModule {

    /**
     * Provides service to be used for communicating with swapi.co endpoints
     */
    @ActivityScope
    @Provides
    @Named(SERVICE_SWAPI)
    fun provideSwapiService(@Named(API_SWAPI) retrofit: Retrofit): SwapiApi {
        return retrofit.create(SwapiApi::class.java)
    }

}