package trivago.starwars.android.core.platform

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey

/**TODO: Notice this about Glide
 * This class is for [Glide]
 *
 * Use [GlideApp] instead of [Glide] when using glide
 * i.e GlideApp.with() instead of Glide.with()
 */
class StarWarsGlideApp: AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        super.applyOptions(context, builder)
        builder.apply { RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).signature(ObjectKey(System.currentTimeMillis().toShort())) }
    }
}