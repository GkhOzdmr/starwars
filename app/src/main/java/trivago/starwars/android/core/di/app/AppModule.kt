package trivago.starwars.android.core.di.app

import android.content.Context
import dagger.Binds
import dagger.Module
import trivago.starwars.android.core.platform.StarWarsApp


/**
 * Dagger2 Module for providing Application wise dependencies
 */
@Module
abstract class AppModule{

    /**
     * Provides context for modules that need context of the application
     *
     * @return Application context
     */
    @Binds
    abstract fun provideContext(application: StarWarsApp): Context

}