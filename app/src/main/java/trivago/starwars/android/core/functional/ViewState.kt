package trivago.starwars.android.core.functional

/**
 * Sealed class to determine the state of view.
 * Use to notify observers for showing/hiding loading indicators etc.
 */
sealed class ViewState {

    /**
     * Used when view should show loading indicator
     * @param message optional message to be displayed
     */
    class ShowProgress(message: String? = ""): ViewState()
    object HideProgress: ViewState()

}