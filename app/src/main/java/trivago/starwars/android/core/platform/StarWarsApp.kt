package trivago.starwars.android.core.platform

import android.content.Context
import androidx.multidex.MultiDex
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import timber.log.Timber
import trivago.starwars.android.BuildConfig
import trivago.starwars.android.core.di.app.DaggerAppComponent

class StarWarsApp: DaggerApplication() {

    /**
     * Dagger2 injector plantation for auto injecting to activities and fragments
     *
     * @see trivago.starwars.android.core.di.app.ActivityBuilder
     * @see trivago.starwars.android.core.di.app.FragmentBuilder
     */
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        plantTimber()
        plantCalligraphy()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        plantMultiDex()
    }

    /**
     * Timber plantation. On release version, Timber logs won't be compiled.
     */
    private fun plantTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    /**
     * Calligraphy 3 plantation.
     */
    private fun plantCalligraphy() {
        ViewPump.init(ViewPump.builder().build())
    }

    /**
     * Installing MultiDex
     */
    private fun plantMultiDex() {
        MultiDex.install(this)
    }

}