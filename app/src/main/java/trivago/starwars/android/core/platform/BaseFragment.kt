package trivago.starwars.android.core.platform

import android.os.Bundle
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import androidx.annotation.AnyRes
import androidx.annotation.StringRes
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import android.view.*
import android.view.View.*
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import trivago.starwars.android.features.MainActivity

/**
 * Base Fragment class with common use functions and helper methods for handling views and back button events.
 */
abstract class BaseFragment : DaggerFragment() {

    abstract fun layoutId(): Int

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    internal val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutId(), container, false)


    /**
     * Handles fragment specific onBackPressed actions. If no action to be done
     * by fragment, method will return true and allow activity to call super.onBackPressed
     *
     * @return allow super.onBackPressed to be called. Return true to allow.
     */
    abstract fun onBackPressed(): Boolean

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

    internal fun showProgress() = progressStatus(VISIBLE)

    internal fun hideProgress() = progressStatus(GONE)

    private fun progressStatus(viewStatus: Int) =
        with(activity) { if (this is MainActivity) this.progress.visibility = viewStatus }

    /**
     * Disposes un-disposed subscriptions, should be called at onStop/onDestroy lifecycle state
     */
    internal fun disposeSubscriptions() {
        if (!disposable.isDisposed) disposable.clear()
    }

    internal fun hideKeyboard() {
        if (view != null) {
            val imm = context!!.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(view!!.rootView.windowToken, 0)
            view!!.clearFocus()
        }
    }

    /**
     * Navigates to specified fragment via JetPack Navigation
     *
     * @param actionId action that is specified in the [nav_graph]
     * @param bundle Optional bundle to navigate to the fragment with
     */
    internal fun navigateTo(@AnyRes actionId: Int, bundle: Bundle?) {
        findNavController().navigate(actionId, bundle)
    }

    /**
     * Pops back to previous fragment in navigation graph
     */
    internal fun popBack() {
        findNavController().popBackStack()
    }

    internal fun showSnackbar(@StringRes message: Int) =
        Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT).show()
}
