package trivago.starwars.android.core.di.app

import dagger.Module
import dagger.android.ContributesAndroidInjector
import trivago.starwars.android.core.di.annotations.ActivityScope
import trivago.starwars.android.core.di.app.viewmodel.ViewModelModule
import trivago.starwars.android.core.di.data.NetworkModule
import trivago.starwars.android.core.di.data.RepositoryModule
import trivago.starwars.android.core.di.data.ServiceModule
import trivago.starwars.android.core.di.domain.UseCaseModule
import trivago.starwars.android.features.character.presentation.fragment.CharDetailsFragment
import trivago.starwars.android.features.character.presentation.fragment.CharSearchFragment

/**
 * Dagger2 Module for providing Fragment instances
 */
@Module
abstract class FragmentBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            RepositoryModule::class,
            ServiceModule::class,
            UseCaseModule::class,
            ViewModelModule::class
        ]
    )
    internal abstract fun bindCharSearchFragment(): CharSearchFragment

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            NetworkModule::class,
            RepositoryModule::class,
            ServiceModule::class,
            UseCaseModule::class,
            ViewModelModule::class
        ]
    )
    internal abstract fun bindCharDetailsFragment(): CharDetailsFragment

}