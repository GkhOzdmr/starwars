package trivago.starwars.android.core.di.app

import dagger.Module
import dagger.android.ContributesAndroidInjector
import trivago.starwars.android.core.di.annotations.ActivityScope
import trivago.starwars.android.core.di.app.viewmodel.ViewModelModule
import trivago.starwars.android.core.di.domain.UseCaseModule
import trivago.starwars.android.features.MainActivity

/**
 * Dagger2 Activity injector
 *
 * Modules that activities use can be injected here
 */
@Module
abstract class ActivityBuilder {

    /**
     * Injects given modules to MainActivity
     */
    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            UseCaseModule::class,
            ViewModelModule::class
        ]
    )
    internal abstract fun bindMainActivity(): MainActivity

}