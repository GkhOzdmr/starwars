package trivago.starwars.android.core.platform

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import dagger.android.support.DaggerAppCompatActivity
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import timber.log.Timber
import trivago.starwars.android.R

/**
 * Base Activity class with helper methods for handling fragment transactions and back button
 * events.
 *
 * @see AppCompatActivity
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    /**
     * Sets the implementor activity's layout id
     *
     * @return activity layout resource id
     */
    abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase!!))
    }

    /**
     * If fragment implementation of onBackPress allows, calls super.onBackPressed()
     */
    override fun onBackPressed() {
        val currentFragment = getCurrentFragment()
        if (currentFragment is BaseFragment) {
            Timber.d("currentFragment is BaseFragment")
            if(currentFragment.onBackPressed()) super.onBackPressed()
        } else {
            Timber.d("else")
            super.onBackPressed()
        }
    }

    private fun getCurrentFragment(): Fragment? {
        val currentNavHost = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        val currentFragmentClassName =
            ((this as AppCompatActivity).findNavController(R.id.nav_host_fragment).currentDestination as FragmentNavigator.Destination)
                .className
        return currentNavHost?.childFragmentManager?.fragments?.filterNotNull()?.find {
            it.javaClass.name == currentFragmentClassName
        }
    }
}
