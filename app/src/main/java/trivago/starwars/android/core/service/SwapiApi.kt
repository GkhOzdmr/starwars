package trivago.starwars.android.core.service

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import trivago.starwars.android.features.character.data.entity.*

/**
 * [Retrofit] interface for communicating with www.swapi.co endpoints
 */
interface SwapiApi {

    @GET("people/")
    fun searchCharacter(@Query("search") name: String): Observable<CharSearchResultEntity>

    @GET("people/{peopleNo}")
    fun getPerson(@Path("peopleNo") peopleNo: String): Observable<CharDetailsEntity>

    @GET("films/{filmNo}")
    fun getFilm(@Path("filmNo") filmNo: String): Observable<FilmDetailsEntity>

    @GET("species/{specieNo}")
    fun getSpecie(@Path("specieNo") specieNo: String): Observable<SpecieDetailsEntity>

    @GET("planets/{planetNo}")
    fun getPlanet(@Path("planetNo") planetNo: String): Observable<PlanetDetailsEntity>

}