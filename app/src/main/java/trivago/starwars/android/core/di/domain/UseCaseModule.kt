package trivago.starwars.android.core.di.domain

import dagger.Module
import dagger.Provides
import trivago.starwars.android.core.di.annotations.ActivityScope
import trivago.starwars.android.features.character.data.repository.CharSearchRepository
import trivago.starwars.android.features.character.domain.usecase.GetPerson
import trivago.starwars.android.features.character.domain.usecase.SearchCharacters

/**
 * Dagger2 Module for providing UseCases
 */
@Module
class UseCaseModule {

    @ActivityScope
    @Provides
    fun provideGetPersonUseCase(charSearchRepository: CharSearchRepository) =
        GetPerson(charSearchRepository)

    @ActivityScope
    @Provides
    fun provideSearchCharactersUseCase(charSearchRepository: CharSearchRepository) =
        SearchCharacters(charSearchRepository)

}